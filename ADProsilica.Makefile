# Copyright (C) 2023 European Spallation Source ERIC

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

where_am_I := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))
include $(E3_REQUIRE_TOOLS)/driver.makefile

EXCLUDE_ARCHS += linux-corei7-poky
EXCLUDE_ARCHS += linux-ppc64e6500

SUPPORT:=prosilicaSupport

APP:=prosilicaApp
APPDB:=$(APP)/Db
APPSRC:=$(APP)/src

## We will use XML2 as the system lib, instead of ADSupport
ifeq (linux-ppc64e6500, $(findstring linux-ppc64e6500,$(T_A)))
USR_INCLUDES += -I$(SDKTARGETSYSROOT)/usr/include/libxml2
else ifeq (linux-corei7-poky, $(findstring linux-corei7-poky,$(T_A)))
USR_INCLUDES += -I$(SDKTARGETSYSROOT)/usr/include/libxml2
else
USR_INCLUDES += -I/usr/include/libxml2
endif

LIB_SYS_LIBS += xml2

USR_CXXFLAGS += -D_LINUX -D_x86

HEADERS += $(SUPPORT)/PvApi.h
SOURCES += $(APPSRC)/prosilica.cpp
DBDS    += $(APPSRC)/prosilicaSupport.dbd

LIBOBJS += $(where_am_I)$(SUPPORT)/os/linux-x86_64/libPvAPI.a

TEMPLATES += $(wildcard $(APPDB)/*.db)
TEMPLATES += $(wildcard $(APPDB)/*.req)

USR_DBFLAGS += -I . -I ..
USR_DBFLAGS += -I $(EPICS_BASE)/db
USR_DBFLAGS += -I $(APPDB)

USR_DBFLAGS += -I $(E3_SITEMODS_PATH)/adcore/$(call FETCH_REVISION_NUMBER,$(E3_SITEMODS_PATH),adcore)/db

SUBS=$(wildcard $(APPDB)/*.substitutions)
TMPS=$(wildcard $(APPDB)/*.template)

.PHONY: vlibs
vlibs:
